const morgan = require('morgan');
const express = require('express');
const fileRouter = require('./fileRouter');
const fs = require('fs');
const path = require('path');

const accessLogStream = fs.createWriteStream(path.join(__dirname, '../files/','server.log'), { flags: 'a' })

module.exports = (app) => {
    app.use(express.json());
    app.use(morgan('combined', { stream: accessLogStream }));
    app.use('/api/files', fileRouter);
};
