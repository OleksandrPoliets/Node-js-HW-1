const { Router } = require('express');
const {
    getFileList,
    checkFile,
    createFile,
    getFileByName,
    updateFileByName,
    isExsistFile,
    deletFiliByName
} = require('../middleWare/fileMiddlewar');
const { createPasswordToken, checkToken } = require('../middleWare/jwtMiddleware');

const router = Router();

router.get('/', getFileList)
    .get('/:filename/:password?', checkToken, isExsistFile, getFileByName)
    .post('/:password?', createPasswordToken, checkFile, createFile)
    .put('/:filename', isExsistFile, updateFileByName)
    .delete('/:filename', isExsistFile, deletFiliByName);

module.exports = router;
