const express = require('express');
const routes = require('./routes/index');
const { createDir } = require('./helpers/fileHelpers');

const startServer = async () => {
    const app = express();
    const PORT = 8080;

    await createDir();
    routes(app);

    app.listen(PORT, () => {
        console.log(`Server listening on port ${PORT}!`);
    });
};

startServer();


