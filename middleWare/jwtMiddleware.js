const createToken = require( '../helpers/jwtHelper');
const { files } = require('../files/fileBase');

const createPasswordToken = (req, res, next) => {
    const { password = false } = req.params;

    if (password) {
        if (password.trim().length === 0 ) {
            return res.status(422).json({
                message: 'Invalid password'
            });
        }
        const tempToken = createToken(password);
        req.body = {
            ...req.body,
            password: tempToken
        }
    }

    next();
}

const checkToken = (req, res, next) => {
    const file = files.find(el => el.filename === req.params.filename);
    const { password: reqPassword = false } = req.params;

    if (file) {
        const { password } = file;
        if (password){
            const tempPass = createToken(reqPassword);

            if (password !== tempPass){
                return res.status(403).json({
                    message: 'Access denied'
                });
            }
        }
    }

    next();
}

module.exports = { createPasswordToken, checkToken }
