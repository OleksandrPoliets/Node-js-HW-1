const fs = require('fs');
const path = require('path');
const { files } = require('../files/fileBase');
const {
    writeFileBase,
    writeFile,
    getFiles,
    readFile,
    updateFile,
    deleteFile
} = require('../helpers/fileHelpers');


const getFileList = async (req, res) => {
    const files = await getFiles();

    if (files.length === 0 ) {
        return res.status(400).json({
            message: 'Client error'
        });
    }

    return res.status(200).json({
        message: 'Success',
        files
    });
}

const checkFile = (req, res, next) => {
    const filesExtensions = ['.log', '.txt', '.json', '.yaml', '.xml', '.js'];
    const { filename = '', content = '' } = req.body;
    const fileExtension = path.extname(filename);
    const supportExtension = filesExtensions.includes(fileExtension);
    const filePath = path.join(__dirname, '../files/usersFiles', filename);
    const isExist = fs.existsSync(filePath);

    if (!filename) {
        return res.status(400).json({
            message: "Please specify 'filename' parameter"
        });
    }

    if (isExist) {
        return res.status(400).json({
            message: 'File is exist'
        })
    }

    if (!supportExtension) {
        return res.status(400).json({
            message: 'Wrong file Format'
        });
    }

    if (content.length === 0 ) {
        return res.status(400).json({
            message: "Please specify 'content' parameter"
        });
    }
    next();
}

const createFile = async (req, res) => {
    const { filename, content, password = false } = req.body;

    files.push({ filename, password });
    try {
        await writeFile(filename, content);
        await writeFileBase({files});

        return res.status(200).json({
            message: 'File created successfully'
        });
    } catch (e) {
        return res.status(500).json({
            message: 'Server error'
        });
    }

}

const isExsistFile = (req, res, next) => {
    const {filename} = req.params || req.body;
    const filePath = path.join(__dirname, '../files/usersFiles', filename);
    const isExist =  fs.existsSync(filePath);

    if (!isExist) {
        return res.status(400).json({
            message: `No file with ${filename} filename found`
        })
    }

    next();
}

const getFileByName = async (req, res) => {
    const {filename} = req.params;

    try {
        const result = await readFile(filename);

        return res.status(200).json({ ...result });
    } catch (e) {
        return res.status(500).json({
            message: 'Server error'
        });
    }

}

const updateFileByName = async (req, res, next) => {
    const { filename } = req.params;
    const { content = '' } = req.body;

    if (!content) {
        return res.status(400).json({
            "message": "Please specify 'content' parameter"
        });
    }

    try {
        await updateFile(filename, content);

        return res.status(200).json({
            message: `File ${filename} updated successfully`
        });
    } catch (e) {
        return res.status(500).json({
            message: 'Server error'
        });
    }
}

const deletFiliByName = async (req, res) => {
    const { filename } = req.params;

    try {
        const tempFiles = files.filter(el => el.filename !== filename);
        console.log(tempFiles);
        await deleteFile(filename);
        await writeFileBase({files: tempFiles});

        return res.status(200).json({
            message: `File ${filename} deleted successfully`
        });
    } catch (e) {
        return res.status(500).json({
            message: 'Server error'
        });
    }

}

module.exports = {
    getFileList,
    checkFile,
    createFile,
    getFileByName,
    updateFileByName,
    isExsistFile,
    deletFiliByName
}
