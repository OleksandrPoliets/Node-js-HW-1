const fs = require('fs').promises;
const path = require('path');

const createDir = async () => {
    const pathToDirr = path.join(__dirname, '../files/');

    await fs.mkdir(path.join(pathToDirr, 'usersFiles'), { recursive: true });
}

const writeFileBase = async content => {
    const filePath = path.join(__dirname, '../files/fileBase.json');

    await fs.writeFile(filePath, JSON.stringify(content), 'utf8');
}

const writeFile = async (FileName, content) => {
    const filePath = path.join(__dirname, '../files/usersFiles', FileName);

    await fs.writeFile(filePath, content, );
}

const getFiles = async () => {
    const filePath = path.join(__dirname, '../files/usersFiles');
    const fileList = await fs.readdir(filePath);

    return fileList
}

const updateFile = async  (fileName, content) => {
    const filePath = path.join(__dirname, `../files/usersFiles/${fileName}`);

    fs.appendFile(filePath, content,'utf8');
}

const readFile = async fileName => {
    const extension = path.extname(fileName);
    const filePath = path.join(__dirname, `../files/usersFiles/${fileName}`);
    const { birthtime } = await fs.stat(filePath);
    const content = await fs.readFile(filePath, 'utf-8');

    return {
        message: 'Success',
        filename: fileName,
        content: content,
        extension,
        uploadedDate: birthtime
    }
}

const deleteFile = async filename => {
    const filePath = path.join(__dirname, `../files/usersFiles/${filename}`);

    await fs.unlink(filePath);
}

module.exports = {
    writeFileBase,
    writeFile,
    getFiles,
    readFile,
    updateFile,
    deleteFile,
    createDir
};
