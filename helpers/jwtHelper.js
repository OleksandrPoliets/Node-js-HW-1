const jwt = require('jsonwebtoken');
const SECRET = 'qweasd';

const createToken = data => jwt.sign(data, SECRET);

module.exports = createToken;
